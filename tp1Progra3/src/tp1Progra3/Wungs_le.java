package tp1Progra3;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.JPanel;

public class Wungs_le {
	private ArrayList <String> palabraIngresada;
	private JFrame frame;
	private Juego juego;
	private JTextField _palabraIngresada ;
	private JButton botonprimerIntento;
	
	private JTextField [][] mtxTextfield;
	private ConfigVisual config;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Wungs_le window = new Wungs_le();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Wungs_le() {
		juego = new Juego();
		palabraIngresada= new ArrayList <String>();
		config = new ConfigVisual();
		mtxTextfield=config.getMatriz();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		new ReglasDeJuego(frame, true).setVisible(true);
		frame = new JFrame();
		frame.getContentPane().setForeground(new Color(0, 128, 192));
		frame.setBounds(100, 100, 570, 417);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.blue);
		frame = new JFrame();
		frame.setBounds(100, 100, 536, 417);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		_palabraIngresada = new JTextField();
		_palabraIngresada.setFont(new Font("Arial Black", Font.PLAIN, 13));
		_palabraIngresada.setBounds(181, 290, 140, 23);
		_palabraIngresada.setColumns(10);
		_palabraIngresada.setEnabled(true);
		frame.getContentPane().add(_palabraIngresada);
		
        botonprimerIntento = new JButton("Agregar Palabra ");
        botonprimerIntento.setBounds(181, 325, 140, 23);
        frame.getContentPane().add(botonprimerIntento);
        
		JLabel titulo = new JLabel(" WORDLE (made in ungs)");
		titulo.setBounds(140, 20, 232, 23);
		frame.getContentPane().add(titulo);
		titulo.setFont(new Font("serif",Font.PLAIN,22));
		
		agregarConfigAlFrame(mtxTextfield);
		botonprimerIntento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int indiceFilas= juego.getIndiceFilas();
				if (juego.palabraConCaracteresValidos(_palabraIngresada.getText()) && 
						juego.elTamañoEsElCorrecto(_palabraIngresada.getText())  ) {
					agregarCaracteres(mtxTextfield[indiceFilas]);
					mostrarcolumnas(mtxTextfield[indiceFilas]);
					if (juego.elJugadorHabraGanado(_palabraIngresada.getText())) {
						new MensajeGanador(frame,true,juego.getPalabraSecreta()).setVisible(true);
						botonprimerIntento.setEnabled(false);
						colorearLosCamposDeTexto(mtxTextfield[indiceFilas]);
					}
					else {
					colorearLosCamposDeTexto(mtxTextfield[indiceFilas]);
					juego.aumentarIndice();
					_palabraIngresada.setText("");
					if(juego.noPuedeSeguirIntentando()) {
						new MensajePerdedor (frame, true,juego.getPalabraSecreta()).setVisible(true);
						botonprimerIntento.setEnabled(false);
						}
					}
					
				} else {
					vaciarLosCamposdeTexto(mtxTextfield[indiceFilas]);
					new ReglasDeJuego (frame, true);
				}
			}

			
		});
		
	}
	
	private void mostrarcolumnas(JTextField [] caracteres) {
		for (int i = 0; i < caracteres.length; i++) {
			System.out.println(caracteres[i].getText());
		}
		
	}
	
	void colorearLosCamposDeTexto(JTextField [] caracteres) {
		for (int i = 0; i < caracteres.length; i++) {
			if (juego.CoincideLosCaracteres(_palabraIngresada.getText().charAt(i), i)) {
				System.out.println("pepe");
				caracteres[i].setBackground(Color.green);
			} else if (juego.laPalabraTieneLaLetra(_palabraIngresada.getText().charAt(i))) {
				caracteres[i].setBackground(Color.yellow);
			}
		}
	}

	void agregarCaracteres(JTextField [] caracteres) {
		for(int i = 0; i < caracteres.length; i++) {
			String s= String.valueOf(_palabraIngresada.getText().charAt(i));
			caracteres[i].setText(s);
		}
		
	}
	
	
	void vaciarLosCamposdeTexto(JTextField [] camposDeTexto) {
		for (int i = 0; i < camposDeTexto.length; i++) {
			camposDeTexto[i].setText("");
		}
	}

	void agregarConfigAlFrame(JTextField[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				frame.getContentPane().add(matriz[i][j]);
			}
		}
	}
	
}

