package tp1Progra3;

import java.awt.Font;
import javax.swing.JTextField;

public class ConfigVisual {
	private JTextField [][] mtxTextfield;


	ConfigVisual(){
		mtxTextfield = new JTextField [6][5];
		configurarPrimeraFila();
		configurarSegundaFila();
		configurarTerceraFila();
		configurarCuartaFila();
		configurarQuintaFila();
		configurarSextaFila();
	}


	private void configurarPrimeraFila() {
		mtxTextfield[0][0] = new JTextField(); 
		mtxTextfield[0][0].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[0][0].setBounds(168, 53, 32, 31);
        mtxTextfield[0][0].setColumns(10);
        mtxTextfield[0][0].setEditable(false);
        
        mtxTextfield[0][1] = new JTextField();
        mtxTextfield[0][1].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[0][1].setBounds(201, 53, 32, 31);
        mtxTextfield[0][1].setColumns(10);
        mtxTextfield[0][1].setEditable(false);
        
        mtxTextfield[0][2] = new JTextField();
        mtxTextfield[0][2].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[0][2].setBounds(234, 53, 32, 31);
        mtxTextfield[0][2].setColumns(10);
        mtxTextfield[0][2].setEditable(false);
       
        mtxTextfield[0][3]= new JTextField();
        mtxTextfield[0][3].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[0][3].setBounds(267, 53, 32, 31);
        mtxTextfield[0][3].setColumns(10);
        mtxTextfield[0][3].setEditable(false);
        
        mtxTextfield[0][4] = new JTextField();
        mtxTextfield[0][4].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[0][4].setBounds(300, 53, 32, 31);
        mtxTextfield[0][4].setColumns(10);
        mtxTextfield[0][4].setEditable(false);
	}
	
	private void configurarSegundaFila() {
		mtxTextfield[1][0] = new JTextField();
		mtxTextfield[1][0].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[1][0].setBounds(168, 90, 32, 31);
        mtxTextfield[1][0].setColumns(10);
        mtxTextfield[1][0].setEditable(false);
		
        mtxTextfield[1][1]= new JTextField();
        mtxTextfield[1][1].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[1][1].setBounds(201, 90, 32, 31);
        mtxTextfield[1][1].setColumns(10);
        mtxTextfield[1][1].setEditable(false);
	
        
        mtxTextfield[1][2] = new JTextField();
        mtxTextfield[1][2].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[1][2].setBounds(234, 90, 32, 31);
        mtxTextfield[1][2].setColumns(10);
        mtxTextfield[1][2].setEditable(false);
	
        mtxTextfield[1][3] = new JTextField();
        mtxTextfield[1][3].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[1][3].setBounds(267, 90, 32, 31);
        mtxTextfield[1][3].setColumns(10);
        mtxTextfield[1][3].setEditable(false);
	
        mtxTextfield[1][4] = new JTextField();
        mtxTextfield[1][4].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[1][4].setBounds(300, 90, 32, 31);
        mtxTextfield[1][4].setColumns(10);
        mtxTextfield[1][4].setEditable(false);
        
	}
	
	private void configurarTerceraFila() {
		mtxTextfield[2][0] = new JTextField();
		mtxTextfield[2][0] .setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[2][0] .setBounds(168, 127, 32, 31);
        mtxTextfield[2][0] .setColumns(10);
        mtxTextfield[2][0] .setEditable(false);
        
        mtxTextfield[2][1] = new JTextField();
        mtxTextfield[2][1].setFont(new Font("Arial Black", Font.PLAIN, 13));
        mtxTextfield[2][1].setBounds(201, 127, 32, 31);
		mtxTextfield[2][1].setColumns(10);
		mtxTextfield[2][1].setEditable(false);

		mtxTextfield[2][2] = new JTextField();
		mtxTextfield[2][2].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[2][2].setBounds(234, 127, 32, 31);
		mtxTextfield[2][2].setColumns(10);
		mtxTextfield[2][2].setEditable(false);
	
		mtxTextfield[2][3] = new JTextField();
		mtxTextfield[2][3].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[2][3].setBounds(267, 127, 32, 31);
		mtxTextfield[2][3].setColumns(10);
		mtxTextfield[2][3].setEditable(false);
	
		mtxTextfield[2][4] = new JTextField();
		mtxTextfield[2][4].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[2][4].setBounds(300, 127, 32, 31);
		mtxTextfield[2][4].setColumns(10);
		mtxTextfield[2][4].setEditable(false);
	
	}

	private void configurarCuartaFila() {
		mtxTextfield[3][0] = new JTextField();
		mtxTextfield[3][0].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[3][0].setBounds(168, 164, 32, 31);
		mtxTextfield[3][0].setColumns(10);
		mtxTextfield[3][0].setEditable(false);
	
		mtxTextfield[3][1] = new JTextField();
		mtxTextfield[3][1].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[3][1].setBounds(201, 164, 31, 31);
		mtxTextfield[3][1].setColumns(10);
		mtxTextfield[3][1].setEditable(false);
	
		mtxTextfield[3][2] = new JTextField();
		mtxTextfield[3][2].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[3][2].setBounds(234, 164, 32, 31);
		mtxTextfield[3][2].setColumns(10);
		mtxTextfield[3][2].setEditable(false);
		
		mtxTextfield[3][3] = new JTextField();
		mtxTextfield[3][3].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[3][3].setBounds(267, 164, 32, 31);
		mtxTextfield[3][3].setColumns(10);
		mtxTextfield[3][3].setEditable(false);
	
		mtxTextfield[3][4] = new JTextField();
		mtxTextfield[3][4].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[3][4].setBounds(300, 164, 32, 31);
		mtxTextfield[3][4].setColumns(10);
		mtxTextfield[3][4].setEditable(false);
	}

	private void configurarQuintaFila() {
		mtxTextfield[4][0] = new JTextField();
		mtxTextfield[4][0].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[4][0].setBounds(168, 201, 32, 31);
		mtxTextfield[4][0].setColumns(10);
		mtxTextfield[4][0].setEditable(false);
	
		mtxTextfield[4][1] = new JTextField();
		mtxTextfield[4][1].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[4][1].setBounds(201, 201, 32, 31);
		mtxTextfield[4][1].setColumns(10);
		mtxTextfield[4][1].setEditable(false);
		
		mtxTextfield[4][2] = new JTextField();
		mtxTextfield[4][2].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[4][2].setBounds(234, 201, 32, 31);
		mtxTextfield[4][2].setColumns(10);
		mtxTextfield[4][2].setEditable(false);
	
		mtxTextfield[4][3] = new JTextField();
		mtxTextfield[4][3].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[4][3].setBounds(267, 201, 32, 31);
		mtxTextfield[4][3].setColumns(10);
		mtxTextfield[4][3].setEditable(false);
	
		mtxTextfield[4][4] = new JTextField();
		mtxTextfield[4][4].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[4][4].setBounds(300, 201, 32, 31);
		mtxTextfield[4][4].setColumns(10);
		mtxTextfield[4][4].setEditable(false);
	}
	
	private void configurarSextaFila() {
		mtxTextfield[5][0] = new JTextField();
		mtxTextfield[5][0].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[5][0].setBounds(168, 238, 32, 31);
		mtxTextfield[5][0].setColumns(10);
		mtxTextfield[5][0].setEditable(false);
		
		mtxTextfield[5][1] = new JTextField();
		mtxTextfield[5][1].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[5][1].setBounds(201, 238, 32, 31);
		mtxTextfield[5][1].setColumns(10);
		mtxTextfield[5][1].setEditable(false);
		
		mtxTextfield[5][2] = new JTextField();
		mtxTextfield[5][2].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[5][2].setBounds(234, 238, 32, 31);
		mtxTextfield[5][2].setColumns(10);
		mtxTextfield[5][2].setEditable(false);
	
		mtxTextfield[5][3] = new JTextField();
		mtxTextfield[5][3].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[5][3].setBounds(267, 238, 32, 31);
		mtxTextfield[5][3].setColumns(10);
		mtxTextfield[5][3].setEditable(false);
		
		mtxTextfield[5][4] = new JTextField();
		mtxTextfield[5][4].setFont(new Font("Arial Black", Font.PLAIN, 13));
		mtxTextfield[5][4].setBounds(300, 238, 32, 31);
		mtxTextfield[5][4].setColumns(10);
		mtxTextfield[5][4].setEditable(false);
	}

	
	public JTextField [][] getMatriz() {
		return mtxTextfield;
	}
	
	


	
	
}