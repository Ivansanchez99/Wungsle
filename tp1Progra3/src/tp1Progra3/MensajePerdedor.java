package tp1Progra3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MensajePerdedor extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private String palabrasecretaSeleccionada;
	private JTextField textField;
	
	public MensajePerdedor(java.awt.Frame parent , boolean modal,String palabraSecreta) {
		setBounds(100, 100, 332, 209);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		JLabel Labeltitulo = new JLabel("Mala suerte che, Perdiste");
		Labeltitulo.setFont(new Font("Serif", Font.BOLD, 14));
		Labeltitulo.setBounds(71, 11, 156, 19);
		contentPanel.add(Labeltitulo);
		palabrasecretaSeleccionada= palabraSecreta;
		
		JLabel labelPalabraSecreta = new JLabel("");
		labelPalabraSecreta.setFont(new Font("Serif", Font.BOLD, 14));
		labelPalabraSecreta.setText("La palabra que tenias que adivinar era :");
		labelPalabraSecreta.setBounds(23, 52, 251, 19);
		contentPanel.add(labelPalabraSecreta);
		
		textField = new JTextField();
		textField.setFont(new Font("Serif", Font.BOLD, 14));
		textField.setBounds(84, 82, 126, 25);
		contentPanel.add(textField);
		textField.setColumns(10);
		textField.setEditable(false);
		{
			textField.setText(palabrasecretaSeleccionada);
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			JButton botonCerrarVentana = new JButton("Ok");
			botonCerrarVentana.setBounds(106, 116, 89, 23);
			contentPanel.add(botonCerrarVentana);
			botonCerrarVentana.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {	
					setVisible(false);
					
				}
			});
		}
	}

}
