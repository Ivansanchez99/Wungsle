package tp1Progra3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

public class MensajeGanador extends JDialog {
	private String palabrasecretaSeleccionada;
	private final JPanel contentPanel = new JPanel();
	private JTextField palabraSeleccionada;
	
	public MensajeGanador(java.awt.Frame parent , boolean modal,String palabraSecreta){
		palabrasecretaSeleccionada=palabraSecreta;
		setBounds(100, 100, 345, 199);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel Labeltitulo = new JLabel("Felicidades!!!, acertaste la palabra");
		Labeltitulo.setFont(new Font("Serif", Font.BOLD, 14));
		Labeltitulo.setBounds(58, 21, 261, 28);
		contentPanel.add(Labeltitulo);
		
		JLabel labelPalabraSecreta = new JLabel("");
		labelPalabraSecreta.setFont(new Font("Serif", Font.BOLD, 14));
		labelPalabraSecreta.setText("La palabra que acertaste es");
		labelPalabraSecreta.setBounds(10, 60, 197, 28);
		contentPanel.add(labelPalabraSecreta);
		
		palabraSeleccionada = new JTextField();
		palabraSeleccionada.setFont(new Font("Serif", Font.BOLD, 14));
		palabraSeleccionada.setBounds(191, 66, 86, 20);
		contentPanel.add(palabraSeleccionada);
		palabraSeleccionada.setColumns(10);
		palabraSeleccionada.setEditable(false);
		
		
		{
			palabraSeleccionada.setText(palabrasecretaSeleccionada);
			
			JButton botonCerrarVentana = new JButton("Ok");
			botonCerrarVentana.setBounds(106, 116, 89, 23);
			contentPanel.add(botonCerrarVentana);
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			botonCerrarVentana.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {	
					setVisible(false);
					
				}
			});
		}
	}
}
