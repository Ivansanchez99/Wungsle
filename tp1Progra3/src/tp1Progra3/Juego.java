package tp1Progra3;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextField;

public class Juego {
	private ArrayList <String > palabrasparaAdivinar; 
	private String palabraSecreta;
	private int indiceFilas;
	
	Juego() {
		palabrasparaAdivinar= new ArrayList<String>() {{
			add("arbol");
			add("panza");
			add("zorro");
			add("clima");
			add("danza");
			add("raton");
			add("perro");
			add("abeja");
			add("burro");
			add("cebra");
			add("cabra");
			add("cerdo");
			add("gallo");
			add("mosca");
			add("pulpo");
			add("cisne");
			add("comer");
			add("cloro");
			add("denso");
			add("dueño");
			add("obeso");
			add("femur");
			add("falso");
			add("letra");
			add("juego");
			add("mente");
			add("marca");
			add("palta");
			add("menso");
			add("nieve");
			add("ojota");
		}}; 
		this.palabraSecreta = palabraAlAzar();
		indiceFilas= 0;

	}
 
	public boolean elTamañoEsElCorrecto (String palabraIngresada) {
		return palabraIngresada.length()==5;
	}
	
	
	public boolean palabraConCaracteresValidos(String  palabraIngresada) {
		boolean tieneCaracterInvalido=true;
		for (int j =0 ;j<palabraIngresada.length();j++) {
			tieneCaracterInvalido = tieneCaracterInvalido && elCaracteresValido (palabraIngresada.charAt(j));
		}
		return tieneCaracterInvalido;
	}
	
	
	public boolean elJugadorHabraGanado(String palabraIngresada) {
		return palabraIngresada.equalsIgnoreCase(palabraSecreta);
	}
	
	
	public boolean CoincideLosCaracteres(char letra ,int indice) {
		return letra == palabraSecreta.charAt(indice);
	}

	public boolean laPalabraTieneLaLetra (char letra) {
		for(int j =0 ; j<palabraSecreta.length();j++) {
			if (letra== palabraSecreta.charAt(j)) {
				return true;
			}
		}
		return false;
	}

	
	boolean elCampodeTextoEstaVacio (String letraingresada) {
		return letraingresada.equals("");
	}
	
	
	boolean huboCoincidencia(String palabra) {
		return palabra.equals(palabraSecreta);
	}


	private boolean elCaracteresValido(char caracter) {
		return (caracter >=97 && caracter<= 122) || (caracter>=65 && caracter<= 90); 
	}

	public String getPalabraSecreta() {
		return palabraSecreta;
	}

	String palabraAlAzar() {
		Random elementoRandom = new Random();
		int valorDado = elementoRandom.nextInt(20);
		return palabrasparaAdivinar.get(valorDado);
		
	}

	void aumentarIndice() {
		indiceFilas++;
	}
	
	public int getIndiceFilas() {
		return indiceFilas;
	}
	
	public boolean noPuedeSeguirIntentando() {
		return indiceFilas==6;
	}
	
}